{
  description = "Spinnaker SDK Camera support";

  inputs.nixpkgs.url = github:NixOS/nixpkgs/nixos-21.11;
  inputs.sipyco.url = github:m-labs/sipyco;  
  inputs.sipyco.inputs.nixpkgs.follows = "nixpkgs";

  outputs = { self, nixpkgs, sipyco }:
    let
      pkgs = import nixpkgs { system = "x86_64-linux"; };
    in rec {

      packages.x86_64-linux = {
        # build sipyco for python3.8
        sipyco = pkgs.python38Packages.buildPythonPackage {
          pname = "sipyco";
          version = "1.2";
          src = pkgs.fetchFromGitHub {
            owner = "m-labs";
            repo = "sipyco";
            rev = "939f84f";
            sha256 = "15Nun4EY35j+6SPZkjzZtyH/ncxLS60KuGJjFh5kSTc=";
          };
          propagatedBuildInputs = with pkgs.python38Packages; [ pybase64 numpy ];
        };
        spinnakersdk = pkgs.python38Packages.buildPythonPackage {
          pname = "spinnakersdk";
          version = "2.6.0.156";
          src = self + "/lib/spinnaker_python-2.6.0.156-cp38-cp38-linux_x86_64.whl";
          propagatedBuildInputs = with pkgs.python38Packages; [ numpy matplotlib pkgs.stdenv.cc.cc.lib pkgs.libusb1 ];
          format = "wheel";
          doCheck = false;
        };
        spinnakercamera = pkgs.python38Packages.buildPythonPackage {
          pname = "spinnakercamera";
          version = "0.1";
          src = self;
          doCheck = false;
          propagatedBuildInputs = [ 
            packages.x86_64-linux.sipyco
            packages.x86_64-linux.spinnakersdk
            (with pkgs.python38Packages; [ setuptools ])
            ];
        };
      };

      defaultPackage.x86_64-linux = packages.x86_64-linux.spinnakercamera;

      devShell.x86_64-linux = pkgs.mkShell {
        name = "spinnakercamera-dev-shell";
        packages = [ pkgs.stdenv.cc.cc.lib pkgs.libusb1 pkgs.zlib pkgs.ffmpeg-full ];
        buildInputs = [
          (
            pkgs.python38.withPackages(ps: [ 
              pkgs.python38Packages.numpy
              pkgs.python38Packages.matplotlib
              pkgs.python38Packages.pandas
              packages.x86_64-linux.spinnakercamera
              packages.x86_64-linux.sipyco
              ])
          )
        ];
        shellHook = ''
echo Entering Spinnaker Camera SDK shell...
export LD_LIBRARY_PATH=${pkgs.stdenv.cc.cc.lib}/lib/:/opt/spinnaker/lib:${pkgs.libusb1}/lib:${pkgs.zlib}/lib:${pkgs.ffmpeg-full}/lib
'';
      };
    };
}