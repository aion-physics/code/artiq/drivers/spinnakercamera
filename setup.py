import setuptools

with open("README.md", "r") as file:
    long_description = file.read()

setuptools.setup(
    name="spinnakercamera",
    version="0.1",
    author="Elliot Bentine",
    description="Python wrapper for Spinnaker SDK cameras",
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=['spinnakercamera'],
    classifiers=[
        "Programming Language :: Python :: 3",
    ],
    python_requires='>=2.7, <4',
    install_requires=[
        'numpy',
    ],
    entry_points={
      "console_scripts": [
         "run_camera = spinnakercamera.run_camera:main",
      ]
    }
)
