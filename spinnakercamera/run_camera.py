# Runs the camera as a server that artiq can connect to.

import argparse
import logging
import sys
import asyncio

from spinnakercamera.camera import SDK, Camera

from sipyco.pc_rpc import Server
from sipyco import common_args

logger = logging.getLogger(__name__)


def get_argparser():
    parser = argparse.ArgumentParser(
        description="""Camera controller for FLIR Spinnaker SDK cameras.""")
    parser.add_argument("--serial-number", default=None,
                        help="Serial number of the camera to connect to.")
    common_args.simple_network_args(parser, 8762)
    common_args.verbosity_args(parser)
    return parser


def main():
    args = get_argparser().parse_args()
    common_args.init_logger_from_args(args)

    if args.serial_number is None:
        logger.error("Please specify a camera serial number using --serial-number.")
        sys.exit(1)

    async def run():
        with SDK() as sdk:
            cam = sdk.get_cam_by_serial_number(args.serial_number)
            if cam is None:
                logger.error("Cannot find camera with serial number={}.".format(args.serial_number))
                sys.exit(1)

            with Camera(cam) as camera:
                logger.info("Connected to camera.")
                server = Server({"camera": camera}, None, True)
                await server.start(common_args.bind_address_from_args(args), args.port)
                try:
                    await server.wait_terminate()
                finally:
                    await server.stop()

            del cam

    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(run())
    except KeyboardInterrupt:
        pass
    finally:
        loop.close()

    if __name__ == "__main__":
        main()
