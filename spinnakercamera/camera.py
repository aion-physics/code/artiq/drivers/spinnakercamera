import logging
import PySpin
import numpy as np

logger = logging.getLogger(__name__)

class Camera:
    """A camera controlled by the Spinnaker SDK."""

    def __init__(self, camera):
        self.camera = camera

    def __enter__(self):
        self.nodemap_tldevice = self.camera.GetTLDeviceNodeMap()
        self.camera.Init()
        self.nodemap = self.camera.GetNodeMap()
        self.sNodemap = self.camera.GetTLStreamNodeMap()
        self.acquiring = False
        return self

    def __exit__(self, exc_type, exc_value, tb):
        self.camera.DeInit()
        # Note: Spinnaker SDK examples explicitly delete the camera rather than rely on disposal.
        cam = self.camera
        self.camera = None
        del cam
        if exc_type is not None:
            return False
        return True

    def serial_number(self):
        """Gets the serial number of the camera."""
        device_serial_number = ''
        node_device_serial_number = PySpin.CStringPtr(self.nodemap_tldevice.GetNode('DeviceSerialNumber'))
        if PySpin.IsAvailable(node_device_serial_number) and PySpin.IsReadable(node_device_serial_number):
            device_serial_number = node_device_serial_number.GetValue()
        return device_serial_number

    def reset(self):
        """
        Puts the camera into a well-defined/deterministic state.
        """
        self.end_acquisition()
        self.configure_trigger()

    def configure_trigger(self, delay_us=28, enabled=True):
        if enabled:
            # must first disable trigger before changing settings.
            #self.camera.TriggerMode.SetIntValue(self.camera.TriggerMode.GetEntryByName('Off').GetValue())
            self.camera.TriggerMode.SetValue(PySpin.TriggerMode_Off)
            self.camera.TriggerSource.SetValue(PySpin.TriggerSource_Line0)
            self.camera.TriggerSource.SetValue(PySpin.TriggerSelector_FrameStart)
            self.camera.TriggerDelay.SetValue(delay_us)
            self.camera.TriggerMode.SetValue(PySpin.TriggerMode_On)
        else:
            self.camera.TriggerMode.SetValue(PySpin.TriggerMode_Off)

    def start_acquisition(self, frame_number=1, exposure_us=None):
        """
        Arms the camera to aquire images.
        
        Keyword arguments:
        nFrames -- Number of frames to acquire, or None for a continuous acquisition.
        exposure_us -- Exposure time in us, or None to use the trigger width.
        """

        # Disable gain, nobody likes gain
        self.camera.GainAuto.SetValue(PySpin.GainAuto_Off)
        self.camera.Gain.SetValue(0)

        # Make sure we read back images oldest first. (Prop is actual on TL)
        # self.camera.StreamBufferHandlingMode.SetIntValue(self.camera.StreamBufferHandlingMode.GetEntryByName('OldestFirst').GetValue())

        # Configure acquisition mode according to frame number - None gives continuous acquisition.
        if frame_number is None:
            self.camera.AcquisitionMode.SetValue(PySpin.AcquisitionMode_Continuous)
        else:
            if frame_number > 1:
                self.camera.AcquisitionMode.SetValue(PySpin.AcquisitionMode_MultiFrame)
                self.camera.AcquisitionFrameCount.SetValue(frame_number)
                self.camera.AcquisitionBurstFrameCount.SetValue(1)
            else:
                self.camera.AcquisitionMode.SetValue(PySpin.AcquisitionMode_SingleFrame)

        # Set the exposure of the camera to be set by the trigger width
        self.camera.ExposureAuto.SetValue(PySpin.ExposureAuto_Off)
        if exposure_us is None:
            self.camera.ExposureMode.SetValue(PySpin.ExposureMode_TriggerWidth)
        else:
            self.camera.ExposureMode.SetValue(PySpin.ExposureMode_Timed)
            self.camera.ExposureTime.SetValue(exposure_us)

        self.camera.BeginAcquisition()
        self.acquiring = True

    def get_frame(self, timeout=1000):
        """"Gets a frame from the camera, or None if there is a timeout."""
        image_data = None
        try:
            image_result = self.camera.GetNextImage(timeout)
            image_data = np.array(image_result.GetNDArray())
        finally:
            image_result.Release()
        return image_data

    def get_acquisition_in_progress(self):
        #return PySpin.CBooleanPtr(self.camera.AcquisitionStatus).GetValue() #self.nodemap.GetNode('AcquisitionStatus')
        # Contrary to what the SDK manual says, there is no 'Acquisition Status' node.
        return self.acquiring

    def end_acquisition(self):
        """Disarms the camera, stopping an aquisition that is currently in progress."""
        if self.get_acquisition_in_progress():
            try:
                self.camera.EndAcquisition()
                self.acquiring = False
            except Exception as e:
                logger.error('Unable to end camera acquisition:')
                logger.error(e)
                

    def list_nodes(self):
        """Lists all nodes available on the camera, and whether they are readable."""
        for node in self.camera.GetNodeMap().GetNodes():
            name = node.GetName()
            readable = "r" if PySpin.IsReadable(node) else "x"
            logger.info('Node ({}): {}'.format(readable, name))

class SDK:
    """Wrapper for the Spinnaker SDK."""

    def __init__(self):
        pass
    
    def __enter__(self):
        self.system = PySpin.System.GetInstance()
        version = self.system.GetLibraryVersion()
        logger.info('SpinnakerSDK version: %d.%d.%d.%d' % (version.major, version.minor, version.type, version.build))
        self.cameras = self.system.GetCameras()
        return self

    def __exit__(self, exc_type, exc_value, tb):
        #Cleanup resources as per the spinnaker examples.
        self.cameras.Clear()
        sys = self.system
        self.system = None
        sys.ReleaseInstance()
        if exc_type is not None:
            return False
        return True
    
    def get_cam_by_serial_number(self, serial_number):
        """Get camera by serial number, which should be specified as a string."""
        # turns out there is a get by serial number function on CameraList - nevermind.
        found = False
        for cam in self.cameras:
            with Camera(cam) as camera:
                if camera.serial_number() == serial_number:
                    found = True
                    break
        if found:
            return cam
        else:
            del cam
        raise Exception("Could not find camera for given serial number")
            
import time

def main():
    with SDK() as sdk:
        logger.info('Found {} cameras.'.format(len(sdk.cameras)))
        for cam in sdk.cameras:
            with Camera(cam) as camera:
                logger.info('Serial Number={}'.format(camera.serial_number()))
                #camera.list_nodes()
                logger.info('Acquisition In Progress={}'.format(camera.get_acquisition_in_progress()))

        del cam # Note: Important! you have to do this otherwise cam reference doesn't get disposed before SDK closes.

        with Camera(sdk.get_cam_by_serial_number('20392727')) as camera:
            time.sleep(0.1)
            camera.start_acquisition()
            time.sleep(2)
            camera.end_acquisition()
            time.sleep(1)

        
        
        

if __name__ == '__main__':
    main()